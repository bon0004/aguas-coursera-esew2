#******************************************************************************
# Copyright (C) 2017 by Alex Fosdick - University of Colorado
#
# Redistribution, modification or use of this software in source or binary
# forms is permitted as long as the files maintain this copyright. Users are 
# permitted to modify this and use it to learn about the field of embedded
# software. Alex Fosdick and the University of Colorado are not liable for any
# misuse of this material. 
#
#*****************************************************************************

# Add your Source files to this variable
SRCS_ASM_HOST =	main.asm \
	memory.asm 


SRCS_PRE_HOST =	main.i \
	memory.i 


SRCS_HOST =	main.c \
	memory.c 

SRCS_ASM_TARGET =	main.asm \
	memory.asm \
	startup_msp432p401r_gcc.asm \
	system_msp432p401r.asm \
	interrupts_msp432p401r_gcc.asm

SRCS_PRE_TARGET =	main.i \
	memory.i \
	startup_msp432p401r_gcc.i \
	system_msp432p401r.i \
	interrupts_msp432p401r_gcc.i

SRCS_TARGET =	main.c \
	memory.c \
	startup_msp432p401r_gcc.c \
	system_msp432p401r.c \
	interrupts_msp432p401r_gcc.c

# Add your include paths to this variable
INCLUDES = \
   -I../include/common \
   -I../include/CMSIS \
   -I../include/msp432 

