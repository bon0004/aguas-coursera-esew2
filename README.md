###**************************************************************************
# author Jose Aguas
# date 2018-7-3
#
# build examples:
#     make memory.o	PLATFORM=MSP432
#     make build	PLATFORM=HOST
#     make compile-all	PLATFORM=MSP432
#     make clean	PLATFORM=MSP432
#
###**************************************************************************

###******************************************************************************
# Copyright (C) 2017 by Alex Fosdick - University of Colorado
#
# Redistribution, modification or use of this software in source or binary
# forms is permitted as long as the files maintain this copyright. Users are 
# permitted to modify this and use it to learn about the field of embedded
# software. Alex Fosdick and the University of Colorado are not liable for any
# misuse of this material. 
#
###*****************************************************************************


